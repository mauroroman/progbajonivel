#include <stdio.h>
#include <malloc.h>


typedef struct Node {
    void* data;
    struct Node *next;
} Node;
typedef struct LinkedList {
    Node *head;
    int size;
} LinkedList;

LinkedList *newList() {
    LinkedList *list = malloc(sizeof(LinkedList));
    list->head=NULL;
    list->size=0;
    return list;
}

LinkedList *addNode(LinkedList *list, void data) {

    Node *newNode = malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    if (list->head == NULL) {
        list->head = newNode;
        list->size = 1;
    } else {
        Node *pointer = list->head;
        while (pointer->next != NULL) {
            pointer = pointer->next;
        }
        pointer->next = newNode;
        list->size += 1;
    }
    return list;
}

int sizeOfList(LinkedList *list) {
    printf("Size: %d", list->size);
    return list->size;
}

int get(LinkedList *list, void *element) {
    Node *header = list->head;
    if (element > list->size) {
        printf("error: elemento inexistente");
    }
    for (int i = 0; i < element; i++) {
        header = header->next;
    }
    return header->data;
}

LinkedList* delete(LinkedList *list, void *element) {
    Node *previous = list->head;
    if (element > list->size) {
        printf("error: elemento inexistente");
    }
    if(element==0){
        LinkedList* newHead = newList();
        newHead->head = previous->next;
        newHead->size = list->size-1;
        free(previous);
        return newHead;
    }else{
        for (int i = 0; i < element-1; i++) {
            previous = previous->next;
        }
        Node *consecutive = previous->next->next;
        free(previous->next);
        previous->next = consecutive;
        list->size -=1;
        return list;}
}

void printList(LinkedList *list) {
    Node *pointer = list->head;
    while (pointer->next != NULL) {
        printf("%d\n", pointer->data);
        pointer = pointer->next;
    }
    printf("%d\n", pointer->data);
}


int main() {
    LinkedList* list = newList();
    addNode(list,2);
    addNode(list,3);
    addNode(list,4);
    addNode(list,5);
    printList(list);
    printf("-----\n");
    printf("%d\n",get(list,0));
    list = delete(list,0);
    printf("-----\n");
    printList(list);
    sizeOfList(list);
    return 0;
}


