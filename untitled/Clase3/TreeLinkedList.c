#include <malloc.h>
#include <stdio.h>


typedef struct Node {
    int data;
    struct Node *left;
    struct Node *right;
} Node;
typedef struct TreeLinkedList {
    Node *head;
    int size;
} TreeLinkedList;

Node *newNode() {
    Node *newNode = malloc(sizeof(Node));
    newNode->left = NULL;
    newNode->right = NULL;
}

TreeLinkedList *newList() {
    TreeLinkedList *list = malloc(sizeof(TreeLinkedList));
    list->head = NULL;
    list->size = 0;
    return list;
}
void downHeap(Node *head, int value) {
    int leafValue = head->data;
    if (value < leafValue) {
        if (head->left != NULL) {
            downStream(head->left, value);
        } else {
            Node* node = newNode();
            node->data = value;
            head->left = node;
        };
    }
    if (value >= leafValue) {
        if (head->right != NULL) {
            downStream(head->right, value);
        } else {
            Node* node = newNode();
            node->data = value;
            head->left = node;
        }
    }
}

void addLeave(TreeLinkedList *list, int value) {
    if (list->head == NULL) {
        Node *newHead = newNode();
        newHead->data = value;
        list->head = newHead;
        list->size = 1;
    } else {
        downStream(list->head, value);
        list->size += 1;
    }
}
int sizeOfList(TreeLinkedList *list) {
    printf("Size: %d", list->size);
    return list->size;
}
