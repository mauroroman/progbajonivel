#include <stdio.h>
#include <string.h>

typedef struct {
    char nombre[100];
    int edad;
    int dni;
}Persona;
void cambiarNombre(Persona per){
    char newname[100];
    printf("Introduce un nombre: ");
    scanf("%s", newname);
    strcpy(per.nombre, newname);
};
void cambiarEdad(Persona per){
    int newAge;
    printf("Introduce una edad: ");
    scanf("%d", newAge);
    per.edad = newAge;
};
void cambiarDni(Persona per){
    int dni;
    printf("Introduce un dni: ");
    scanf("%08d", dni);
    per.dni = dni;
};
int main(){
    Persona p = {"Juan", 25, 12345678};

    printf("Nombre: %s\n", p.nombre);
    cambiarNombre(&p);
    printf("Nuevo nombre: %s\n", p.nombre);

    printf("Edad: %d\n", p.edad);
    cambiarEdad(&p);
    printf("Nueva edad: %d\n", p.edad);

    printf("DNI: %08d\n", p.dni);
    cambiarDni(&p);
    printf("Nuevo DNI: %08d\n", p.dni);

    return 0;
}