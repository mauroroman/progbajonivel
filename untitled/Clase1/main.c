#include <stdio.h>

int ejercicio1() {
    char nombre[100];
    int edad;
    printf("Ingrese su nombre: ");
    scanf("%s", nombre);
    printf("Ingrese su edad: ");
    scanf("%d", &edad);
    printf("Su nombre es %s y su edad es %d", nombre, edad);
    return 0;
};

int ejercicio2() {
    int num[] = {12, 3, 4, 56, 7, 1, 36, 78, 423, 0, 577, 125875};
    int max = 0;
    for (int i = 0; i < sizeof(num) / sizeof(int); i++) {
        if (num[i] > max) {
            max = num[i];
        }
    }
    printf("Max= %d", max);
    return 0;
};

int ejercicio3() {
    int num[] = {12, 3, 4, 56, 7, 1, 36, 78, 423, -7, 577, 8};
    int min = 0;
    for (int i = 0; i < sizeof(num) / sizeof(int); i++) {
        if (num[i] < min) {
            min = num[i];
        }
    }
    printf("Min= %d", min);
    return 0;
};

int ejercicio4() {
    int num[3];
    printf("Ingrese 3 numeros: ");
    scanf("%d %d %d", &num[0], &num[1], &num[2]);
    int sum = 0;
    for (int i = 0; i < sizeof(num) / sizeof(int); i++) {
        sum += num[i];
    }
    float prom = (float) sum / 3;
    printf("Prom= %f", prom);
    return 0;
};

int ejercicio5() {
    for (int i = 0; i < 128; i++) {
        printf("%03d-->%c \n", i, i);
    }
    return 0;
}

int ejercicio6() {
    int num;
    printf("Ingrese un numero: ");
    scanf("%d", &num);
    if (num % 2 == 0) {
        printf("Par");
    } else {
        printf("Impar");
    }
    return 0;
}

int ejercicio7() {
    int op;
    printf("    Menu\nSeleccione una opcion: \n1. Op1\n2. Op2\n3. Op3\n4. Salir\n");
    scanf("%d", &op);
    if (op == 1) {
        printf("1!");
    } else {
        if (op == 2) {
            printf("2!");
        } else {
            if (op == 1) {
                printf("3!");
            } else {
                if (op == 4) {
                    printf("Chau!");
                    return 0;
                } else {
                    printf("Comando erroneo!!!!\n");
                    ejercicio7();
                }
            }
        }
    }
    return 0;
};

int main() {
    return ejercicio7();
};